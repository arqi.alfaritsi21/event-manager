import React, { useState } from 'react'
import './Form.css';
import { Row, Col, Input, Upload, Button, DatePicker, Image } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import axios from 'axios';



const { TextArea } = Input;




function Form({ setPage, getDataEvent }) {

  const [title, setTitle] = useState('');
  const [location, setLocation] = useState('');
  const [participant, setParticipant] = useState('');
  const [date, setDate] = useState('');
  const [note, setNote] = useState('');
  const [image, setImage] = useState('');

  const createEvent = async () => {
    let body = {
      title: title,
      location: location,
      participant: participant,
      date: date,
      note: note,
      image: image,
    }
    console.log(body);
    try {
      await axios.post('http://localhost:3001/', body)
      await getDataEvent()
      setPage(1)

    } catch (e) {
      console.log(e);
    }
  }

  const props = {
    name: 'file',
    action: 'http://localhost:3001/upload',
    maxCount: 1,
    onChange(info) {
      if (info.file.status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === 'done') {
        console.log(`${info.file.name} file uploaded successfully`);
        setImage(`${info.file.response.data.data}`)
      } else if (info.file.status === 'error') {
        console.log(`${info.file.name} file upload failed.`);
      }
    },
  };

  return (
    <>
      <Row className="container-form">
        <Col span={12} className="form">
          <div>
            <p className="header-title">ADD NEW EVENT</p>

            <Row justify="space-around">
              <Col span={10}><p className="title-input">Title :</p></Col>
              <Col span={10}><p className="title-input">Location :</p></Col>
            </Row>
            <Row justify="space-around">
              <Col span={10}><Input onChange={(e) => setTitle(e.target.value)} autoFocus placeholder="Input Title" /></Col>
              <Col span={10}><Input onChange={(e) => setLocation(e.target.value)} placeholder="Input Location" /></Col>
            </Row>


            <Row justify="space-around">
              <Col span={10}><p className="title-input">Participant :</p></Col>
              <Col span={10}><p className="title-input date">Date :</p></Col>
            </Row>
            <Row justify="space-around">
              <Col span={10}><Input onChange={(e) => setParticipant(e.target.value)} placeholder="Input Participant" /></Col>
              <Col span={10}><DatePicker onChange={setDate} /></Col>
            </Row>


            <Row justify="space-around">
              <Col span={22}><p className="title-input">Note :</p></Col>
            </Row>
            <Row justify="space-around">
              <Col span={22}><TextArea onChange={(e) => setNote(e.target.value)} block autoSize={{ minRows: 3, maxRows: 5 }} placeholder="Input the Description" allowClear /></Col>
            </Row>

            <div className="upload">
              <Upload {...props} >
                <Button icon={<UploadOutlined />}>Upload</Button>
              </Upload>
            </div>

            <div className="submit">
              <Button onClick={() => { createEvent() }} block type="primary">Submit</Button>

            </div>

          </div>
        </Col>
        <Col span={12} className="image">

          <div className="images">
            <Image.PreviewGroup>
              <Image
                width={200}
                src="https://gw.alipayobjects.com/zos/rmsportal/KDpgvguMpGfqaHPjicRK.svg"
              />
            </Image.PreviewGroup>
          </div>
        </Col>
      </Row>
    </>
  )
}

export default Form
