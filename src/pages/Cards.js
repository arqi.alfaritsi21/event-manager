import React, { useEffect, useState } from 'react'
import './Cards.css';
import { Card, Divider, Row, Col } from 'antd';


const Cards = (data) => {
  console.log(data);
  return (
    <div>
      <Card
        hoverable
        style={{ width: 240 }}
        className="container-card"
      >
        <img alt="example" className="image" src={"http://localhost:3001/" + data.data.image} />
        <p className="location-text">{data.data.location}</p>
        <p className="title-text">{data.data.title}</p>
        <p className="date-text">{data.data.date}</p>
        <Divider className="divider" />

        <div className="participant-container">
          <p className="participant-text">{data.data.participant}</p>

          {/* <Row justify="space-around">
            <Col span={4}><p className="participant-text">Rio</p></Col>
            <Col span={4}><p className="participant-text">Rio</p></Col>
            <Col span={4}><p className="participant-text">Rio</p></Col>
          </Row> */}

        </div>
        <Divider className="divider" />

        <p className="note-title">Note :</p>
        <p className="note-text">{data.data.note}</p>


      </Card>
    </div>
  )
}

export default Cards
