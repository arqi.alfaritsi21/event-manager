import React, { useState, useEffect } from 'react'
import './Tables.css';
import { Table, Input, Space } from 'antd';
import axios from 'axios';


const Tables = () => {

  const [dataEvent, setDataEvent] = useState([]);


  useEffect(() => {
    getDataEvent()
  }, [])

  const getDataEvent = async () => {
    let dataEvent = await axios.get('http://localhost:3001/')
    let tables = dataEvent.data.data
    let dataTable = []
    for (let i = 0; i < tables.length; i++) {
      dataTable.push({
        number: `${tables[i].id}`,
        title: `${tables[i].title}`,
        location: `${tables[i].location}`,
        date: `${tables[i].date}`,
        participant: `${tables[i].participant}`,
        note: `${tables[i].note}`,
      });
    }
    setDataEvent(dataTable)
  }

  const search = async (value) => {
    let body = {
      data: value
    }
    console.log(body);
    try {
      let searchData = await axios.post('http://localhost:3001/search', body)
      console.log(searchData);
      let tables = searchData.data.data
      let dataTable = []
      for (let i = 0; i < tables.length; i++) {
        dataTable.push({
          number: `${tables[i].id}`,
          title: `${tables[i].title}`,
          location: `${tables[i].location}`,
          date: `${tables[i].date}`,
          participant: `${tables[i].participant}`,
          note: `${tables[i].note}`,
        });
      }
      setDataEvent(dataTable)
    } catch (e) {
      console.log(e);
    }
  }


  const { Search } = Input;

  const onSearch = (e) => search(e);

  const columns = [
    {
      title: 'No.',
      dataIndex: 'number',
    },
    {
      title: 'Title',
      dataIndex: 'title',
    },
    {
      title: 'Location',
      dataIndex: 'location',
    },
    {
      title: 'Date',
      dataIndex: 'date',
    },
    {
      title: 'Participant',
      dataIndex: 'participant',
    },
    {
      title: 'Note',
      dataIndex: 'note',
    },
  ];

  return (
    <div>
      <Space direction="vertical">
        <Search placeholder="input search text" onSearch={onSearch} style={{ width: 200 }} />
      </Space>

      <div style={{ marginBottom: 16 }}>
        <span style={{ marginLeft: 8 }}>
        </span>
      </div>
      <Table columns={columns} dataSource={dataEvent} pagination={{
        pageSize: 5
      }} />
    </div>
  );
}

export default Tables
