import React, { useState, useEffect } from 'react';
import './App.css';
import { Row, Col } from 'antd';
import 'antd/dist/antd.css';
import Card from './pages/Cards'
import Tables from './pages/Tables'
import Form from './pages/Form';
import axios from 'axios';


function App() {

  const [dataEvent, setDataEvent] = useState([]);
  const [page, setPage] = useState(1);


  useEffect(() => {
    getDataEvent()
  }, [])

  const getDataEvent = async () => {
    let dataEvent = await axios.get('http://localhost:3001/')
    setDataEvent(dataEvent.data.data)
  }



  return (
    <div className="App">
      <>

        <Row className="navbar">
          <Col span={4} xs={{ order: 1 }} sm={{ order: 1 }} md={{ order: 3 }} lg={{ order: 4 }}>
            <p className="navbar-text" onClick={() => setPage(2)}>Dashboard</p>
          </Col>
          <Col span={4} xs={{ order: 1 }} sm={{ order: 1 }} md={{ order: 4 }} lg={{ order: 3 }}>
            <p className="navbar-text" onClick={() => setPage(3)}>+ Add Event</p>
          </Col>
          <Col span={16} xs={{ order: 4 }} sm={{ order: 4 }} md={{ order: 2 }} lg={{ order: 1 }}>
            <p className="navbar-text  navbar-text-menu" onClick={() => setPage(1)}>EVENT MANAGER</p>
          </Col>
        </Row>

        <div className="container-body">
          {
            page === 1 && <Row justify="space-around">
              {
                dataEvent && dataEvent.length && dataEvent.map((item) => {
                  return <Col span={5}><Card data={item} /></Col>
                })
              }
            </Row>
          }

          {
            page === 2 && <Tables data={dataEvent} />
          }

          {
            page === 3 && <Form setPage={setPage} getDataEvent={getDataEvent} />
          }

        </div>
      </>

    </div>
  );
}

export default App;
